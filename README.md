# Identity-based PSK in WPA2-PSK and WPA3-SAE networks

The following codebase represents a proof of concept for my bachelor thesis.

It is a fork from hostapd version 2.9

The code was modified to support per-device PSKs which are generated at runtime.
It is a stateless protocoll adaption, where identity-based PSKs don't need to be stored in a PSK file. 
