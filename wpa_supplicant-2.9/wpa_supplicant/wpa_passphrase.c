/*
 * WPA Supplicant - ASCII passphrase to WPA PSK tool
 * Copyright (c) 2003-2005, Jouni Malinen <j@w1.fi>
 *
 * This software may be distributed under the terms of the BSD license.
 * See README for more details.
 */

#include "includes.h"

#include "common.h"
#include "crypto/sha1.h"

#include "utils/base64.h"
#include "crypto/sha512.h"

#define MAX_PASSPHRASE_LEN 63

int isValidMacAddress(const char* mac) {
    int i = 0;
    int s = 0;

    while (*mac) {
       if (isxdigit(*mac)) {
          i++;
       }
       else if (*mac == ':') {

          if (i == 0 || i / 2 - 1 != s)
            break;
          ++s;
       }
       else {
           s = -1;
       }
       ++mac;
    }
    return (i == 12 && s == 5);
}

u8 * mac_str2bytes(const char * mac_str) {
	u8 * mac_addr = os_zalloc(ETH_ALEN);
	for (int cnt = 0; cnt < 6; cnt++) {
		sscanf(mac_str, "%2hhx", &mac_addr[cnt]);
		// +2 for the two hex digits and +1 for the ':'
		mac_str += 3;
	}
	return mac_addr;
}

u8 * derive_ibp(u8 * key, size_t key_len, const u8 * mac_addr, u8 * ssid, size_t ssid_len) {

	u8 hmac[SHA512_MAC_LEN + 1];
	u8 stretched_hmac[48];
	u8 * ibp;
	size_t ibp_len;

	int ret = hmac_sha512(key, key_len, mac_addr, ETH_ALEN, hmac);
	if (ret < 0) {
		printf("HMAC failure during identity-based passphrase derivation");
		return NULL;
	}
	// PKCS5_PBKDF2_HMAC_SHA1 wants a cont char *
	hmac[SHA512_MAC_LEN] = '\0';

	ret = pbkdf2_sha1((const char *) hmac, ssid, ssid_len, 4096, stretched_hmac, 48);
	if (ret < 0) {
		printf("PBKDF2 failure during identity-based passphrase derivation");
		return NULL;
	}
	// base64 encode our stretched HMAC. HMAC has 48 byte, base64 encoding has 64 byte
	ibp = base64_encode(stretched_hmac, sizeof(stretched_hmac), &ibp_len);
	if (ibp == NULL) {
		printf("Base64 failure during identity-based passphrase derivation");
		return NULL;
	}
	return ibp;
}

int main(int argc, char *argv[])
{
	unsigned char psk[32];
	int i;
	int use_mod = 0;
	char *ssid, *passphrase, *mac_addr, buf[64], *pos;
	size_t len;

	if (argc < 2) {
		printf("usage: wpa_passphrase <ssid> [passphrase] [mac address]\n"
			"\nIf passphrase is left out, it will be read from "
			"stdin\n");
		return 1;
	}

	ssid = argv[1];

	if (argc == 3) {
		passphrase = argv[2];
	} else if (argc > 3) {
		passphrase = argv[2];
		mac_addr = argv[3];
		use_mod = 1;
	} else {
		printf("# reading passphrase from stdin\n");
		if (fgets(buf, sizeof(buf), stdin) == NULL) {
			printf("Failed to read passphrase\n");
			return 1;
		}
		buf[sizeof(buf) - 1] = '\0';
		pos = buf;
		while (*pos != '\0') {
			if (*pos == '\r' || *pos == '\n') {
				*pos = '\0';
				break;
			}
			pos++;
		}
		passphrase = buf;
	}

	len = os_strlen(passphrase);
	if (len < 8 || len > 63) {
		printf("Passphrase must be 8..63 characters\n");
		return 1;
	}
	if (has_ctrl_char((u8 *) passphrase, len)) {
		printf("Invalid passphrase character\n");
		return 1;
	}

	if(use_mod) {
		if(!isValidMacAddress(mac_addr)) {
			printf("Invalid MAC address\n");
			return 1;
		}

		u8 *mac = mac_str2bytes(mac_addr);
		u8 *ibp = derive_ibp((u8 *)passphrase, os_strlen(passphrase), mac, (u8 *) ssid, os_strlen(ssid));

		// use the first 63 bytes of the hmac
		ibp[MAX_PASSPHRASE_LEN] = '\0';
		passphrase = os_strdup((char *)ibp);
		os_free(mac);
		os_free(ibp);
	}

	pbkdf2_sha1(passphrase, (u8 *) ssid, os_strlen(ssid), 4096, psk, 32);

	printf("network={\n");
	printf("\tssid=\"%s\"\n", ssid);
	printf("\t#psk=\"%s\"\n", passphrase);
	printf("\tpsk=");
	for (i = 0; i < 32; i++)
		printf("%02x", psk[i]);
	printf("\n");
	printf("}\n");

	return 0;
}
